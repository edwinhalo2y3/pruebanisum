package co.com.nisum.microservice.utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ValidationRegularExpression {

    @Value("${expresion-regular.email}")
    private String emailRegx;

    @Value("${expresion-regular.password}")
    private String passwordRegx;

    public boolean isValidEmail(String email) {
	Pattern pattern = Pattern.compile(emailRegx);   
	Matcher matcher = pattern.matcher(email);  
	return matcher.matches();  
    }
    
    public boolean isValidPassword(String password) {
	Pattern pattern = Pattern.compile(passwordRegx);   
	Matcher matcher = pattern.matcher(password);  
	return matcher.matches();  
    }

}
